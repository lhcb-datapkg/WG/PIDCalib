from TeslaTools.TeslaMonitor import TeslaH1, TeslaH2, TeslaH3
from Configurables import DaVinci
#from PidCalibProduction import StandardOfflineRequirements as StdCut

from PidCalibProduction import StandardOfflineRequirements as StdCut

#cut = "(MAXTREE ( TRGHP , ISBASIC ) < 0.4)"

cut         = StdCut.TrackGhostProb
DstCut      = StdCut.DstCut
PosId       = StdCut.PosId
NegId       = StdCut.NegId
Jpsiee      = StdCut.Jpsiee
Jpsi_Pos    = StdCut.Jpsi_Pos
Jpsi_Neg    = StdCut.Jpsi_Neg
BJpsi_Pos   = StdCut.BJpsi_Pos
BJpsi_Neg   = StdCut.BJpsi_Neg
Dsphi_Pos   = StdCut.Dsphi_Pos
Dsphi_Neg   = StdCut.Dsphi_Neg
Dsphi       = StdCut.Dsphi
OmegaCut    = StdCut.OmegaCut

Jpsi_Pos_lowpt    = StdCut.Jpsi_Pos_lowpt
Jpsi_Neg_lowpt    = StdCut.Jpsi_Neg_lowpt
Jpsi_lowpt      = StdCut.Jpsi_lowpt

#EProbe_0Brem = "(PINFO ( LHCb.Particle.HasBremAdded ,  0. ) == 0)"
#ETag_0Brem   = "(PINFO ( LHCb.Particle.HasBremAdded ,  0. ) == 0)"
#EProbe_1Brem = "(PINFO ( LHCb.Particle.HasBremAdded ,  0. ) == 1)"
#ETag_1Brem   = "(PINFO ( LHCb.Particle.HasBremAdded ,  0. ) == 1)"

#Jpsiee_Pos_0Brem  =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = EProbe_0Brem, tag = ETag_0Brem )
#Jpsiee_Pos_1Brem  =  "&" + "(" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = EProbe_1Brem, tag = ETag_0Brem ) + "|" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = EProbe_0Brem, tag = ETag_1Brem ) + ")"
#Jpsiee_Pos_2Brem  =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = EProbe_1Brem, tag = ETag_1Brem )

#Jpsiee_Neg_0Brem  =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = EProbe_0Brem, tag = ETag_0Brem )
#Jpsiee_Neg_1Brem  =  "&" + "(" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = EProbe_1Brem, tag = ETag_0Brem ) + "|" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = EProbe_0Brem, tag = ETag_1Brem ) + ")"
#Jpsiee_Neg_2Brem  =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = EProbe_1Brem, tag = ETag_1Brem )

#cut = "(MAXTREE ( TRGHP , ISBASIC ) < 0.4)"

dv = DaVinci()
dv.DataType = "2018"
dv.EvtMax   = -1
dv.RootInTES = '/Event/Turbo'
dv.InputType= "MDST"
dv.Turbo = True
dv.HistogramFile = "histo.root"

sequence =  [

#============ Kaons - Pions ===============================#

    # Primary kaons-pions: DSt vs Dz Pos
    # positive tag                                                      
    TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + PosId,
              histname = "DstDz_Pos").getAlgorithm()
    # DSt vs Dz Neg
    # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + NegId, 
              histname = "DstDz_Neg").getAlgorithm()

    # Secondary pions: KS0
    # positive tag                                                      
    , TeslaH1 ( "Hlt2PIDKs2PiPiLLTurboCalib:KS0:M",                       
                ";m(#pi^{+}#pi^{-}) [MeV/c^{2}]; Candidates",                               
                1000, 470, 525, cut, histname = "KS0LL").getAlgorithm() 

    # Secondary kaons: D_s+ -> phi(1020) pi+               
    # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, cut + PosId, 
                histname = "DsPhiKK_Neg").getAlgorithm()
                                                                                        
    # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, cut + NegId, 
                histname = "DsPhiKK_Pos").getAlgorithm()

    # Secondary pions: KS0 Down
    , TeslaH1 ( "Hlt2PIDKs2PiPiDDTurboCalib:KS0:M",                       
                ";m(#pi^{+}#pi^{-}) [MeV/c^{2}]; Candidates",                               
                1000, 470, 525, cut, histname = "KS0DD").getAlgorithm() 

    #Secondary kaons: Omega->Lambda K
    , TeslaH1 ( "Hlt2PIDOmega2LambdaKLLLTurboCalib:Omega-:DTF_FUN(M,False,'Lambda0')",
                ";m(#LambdaK^{-}) [MeV/c^{2}]",
                140*5, 1650, 1700, cut + " & " + OmegaCut + NegId,
                histname = "OmegaLLL_Neg").getAlgorithm()

    , TeslaH1 ( "Hlt2PIDOmega2LambdaKLLLTurboCalib:Omega-:DTF_FUN(M,False,'Lambda0')",
                ";m(#LambdaK^{-}) [MeV/c^{2}]",
                140*5, 1650, 1700, cut + " & " + OmegaCut + PosId,
                histname = "OmegaLLL_Pos").getAlgorithm()

    , TeslaH1 ( "Hlt2PIDOmega2LambdaKDDDTurboCalib:Omega-:DTF_FUN(M,False,'Lambda0')",
                ";m(#LambdaK^{-}) [MeV/c^{2}]",
                140*5, 1650, 1700, cut + " & " + OmegaCut + NegId,
                histname = "OmegaDDD_Neg").getAlgorithm()

    , TeslaH1 ( "Hlt2PIDOmega2LambdaKDDDTurboCalib:Omega~+:DTF_FUN(M,False,'Lambda0')",
                ";m(#LambdaK^{+}) [MeV/c^{2}]",
                140*5, 1650, 1700, cut + " & " + OmegaCut + PosId,
                histname = "OmegaDDD_Pos").getAlgorithm()

#============ Muons ===============================#

    # Primary Muons: Jpsi
    # Negative Tag (positive probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
                1000, 3096 - 110, 3096 + 110,cut+Jpsi_Pos, histname = "Jpsi_Pos").getAlgorithm()
    # Positive Tag (negative probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
                1000, 3096 - 110, 3096 + 110,cut+Jpsi_Neg, histname = "Jpsi_Neg").getAlgorithm()

    # Negative Tag (positive probe) with no pt cut
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
                1000, 3096 - 110, 3096 + 110,cut+Jpsi_Pos_lowpt, histname = "Jpsi_noptcut_Pos").getAlgorithm()
    # Positive Tag (negative probe) with no pt cut
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
                1000, 3096 - 110, 3096 + 110,cut+Jpsi_Neg_lowpt, histname = "Jpsi_noptcut_Neg").getAlgorithm()

    # Secondary muons: B -> J/psi (mumu) K
    # 2D histograms including partially
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut+BJpsi_Neg, histname = "BJpsi_Neg").getAlgorithm()
    
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut+BJpsi_Pos, histname = "BJpsi_Pos").getAlgorithm()

    # 2D histograms with no partially
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)'):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5170, 5400, 90*5, 3000, 3200,
                cut+BJpsi_Neg, histname = "BJpsi_NoPR_DTF_Neg").getAlgorithm()
    
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)'):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5170, 5400, 90*5, 3000, 3200,
                cut+BJpsi_Pos, histname = "BJpsi_NoPR_DTF_Pos").getAlgorithm()
    
    #Secondary muons: Ds -> Phi(mu mu) pi
    # positive tag                                                    
    , TeslaH1 ( "Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalib:D_s+:M",
                ";m(#phi#pi^{+}) [MeV/c^{2}]; Candidates",
                140*5, 1900, 2040, cut + PosId + Dsphi_Pos + Dsphi,
                histname = "DsPhiMuMu_Neg").getAlgorithm()
    
    # negative tag                                                      
    , TeslaH1 ( "Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalib:D_s+:M",
                ";m(#phi#pi^{+}) [MeV/c^{2}]; Candidates",
                140*5, 1900, 2040, cut + NegId + Dsphi_Neg + Dsphi,
                histname = "DsPhiMuMu_Pos").getAlgorithm()

#============ Protons ===============================#

    # Lambda0
    # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0LL_Pos").getAlgorithm()
    # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0HPTLL_Pos").getAlgorithm()
    # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0VHPTLL_Pos").getAlgorithm()

    # Lambda~0
    # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0LL_Neg").getAlgorithm()
    # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0HPTLL_Neg").getAlgorithm()
    # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0VHPTLL_Neg").getAlgorithm()

    # Lambda isMuon
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLisMuonTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0LLisMuon_Pos").getAlgorithm()
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLisMuonTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0LLisMuon_Neg").getAlgorithm()

    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
#    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_b0:CHILD(M,1)",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "LbLc_Pos" ).getAlgorithm()

    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
#    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_b0:CHILD(M,1)",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "LbLc_Neg" ).getAlgorithm()

    # Lambda_c -> ^p K pi (inclusive)
    , TeslaH1 ( "Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "Lc_Pos" ).getAlgorithm()

    # Lambda_c -> ^p K pi (inclusive)
    , TeslaH1 ( "Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "Lc_Neg" ).getAlgorithm()

    # Lambda0 Down
    # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiDDTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0DD_Pos").getAlgorithm()
    # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiDDhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0HPTDD_Pos").getAlgorithm()
    # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiDDveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0VHPTDD_Pos").getAlgorithm()

    # Lambda~0 Down
    # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiDDTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0DD_Neg").getAlgorithm()
    # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiDDhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0HPTDD_Neg").getAlgorithm()
    # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiDDveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
                1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0VHPTDD_Neg").getAlgorithm()

#============ Electrons ===============================
    
    # B -> J/psi (ee) K
    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Pos").getAlgorithm()

    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Neg").getAlgorithm()

    # B -> J/psi (ee) K USING DELTA MASS
    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M-CHILD(M,1):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_deltaPos").getAlgorithm()

    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M-CHILD(M,1):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_deltaNeg").getAlgorithm()

    ####### DTF mass w/o contraints --> to "solve" (it simply recovers Brem) the electron issue in 2016 #########

    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,False):DTF_FUN(CHILD(1,M),False)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Pos_DTF").getAlgorithm()
    
    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,False):DTF_FUN(CHILD(1,M),False)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Neg_DTF").getAlgorithm()            

    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,False)-DTF_FUN(CHILD(1,M),False):DTF_FUN(CHILD(1,M),False)",
               ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
               20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
               cut + Jpsiee, histname = "BJpsi_deltaPos_DTF").getAlgorithm()

    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,False)-DTF_FUN(CHILD(1,M),False):DTF_FUN(CHILD(1,M),False)",
               ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
               20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
               cut + Jpsiee, histname = "BJpsi_deltaNeg_DTF").getAlgorithm()
                    
    ####### DTF mass w Jpsi and PV contraints --> Needed for the 1D fit #########

    # negative tag
    #, TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                #";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                #20*10, 4300, 5800, cut + Jpsiee, histname = "BJpsiK_Pos_DTF_PV").getAlgorithm()

    # positive tag
    #, TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                #";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                #20*10, 4300, 5800, cut + Jpsiee, histname = "BJpsiK_Neg_DTF_PV").getAlgorithm()

    # BREM CATEGORIES FOR TOT BREM NUMBER
    # negative tag (0 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_0Brem, histname = "BJpsiK_Pos_DTF_PV_0Brem").getAlgorithm()
    # negative tag (1 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_1Brem, histname = "BJpsiK_Pos_DTF_PV_1Brem").getAlgorithm()
    # negative tag (2 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_2Brem, histname = "BJpsiK_Pos_DTF_PV_2Brem").getAlgorithm()
    # positive tag (0 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_0Brem, histname = "BJpsiK_Neg_DTF_PV_0Brem").getAlgorithm()
    # positive tag (1 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_1Brem, histname = "BJpsiK_Neg_DTF_PV_1Brem").getAlgorithm()
    # positive tag (2 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_2Brem, histname = "BJpsiK_Neg_DTF_PV_2Brem").getAlgorithm()

    # BREM CATEGORIES FOR PROBE BREM NUMBER
    # negative tag (probe 0 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_Probe0Brem, histname = "BJpsiK_Pos_DTF_PV_Probe0Brem").getAlgorithm()
    # negative tag (probe 1 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_Probe1Brem, histname = "BJpsiK_Pos_DTF_PV_Probe1Brem").getAlgorithm()
    # positive tag (probe 0 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_Probe0Brem, histname = "BJpsiK_Neg_DTF_PV_Probe0Brem").getAlgorithm()
    # positive tag (probe 1 Brem)
    , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_Probe1Brem, histname = "BJpsiK_Neg_DTF_PV_Probe1Brem").getAlgorithm()
    
]

#since mass shape and background changes with momentum, do 5 bins
pbins = [0, 11500, 20000, 30000, 50000, 1e9] #roughly isopopulated
for ibin in range(len(pbins)-1):
    pcut_neg = "& ( INTREE ( (Q < 0) & (P > {}) & (P < {} ) & ('e-' == ABSID) ) )".format(pbins[ibin], pbins[ibin+1])
    pcut_pos = "& ( INTREE ( (Q > 0) & (P > {}) & (P < {} ) & ('e-' == ABSID) ) )".format(pbins[ibin], pbins[ibin+1])
    addhistos = [
        # negative tag (probe 0 Brem)
        TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                    ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                    20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_Probe0Brem + pcut_pos, histname = "BJpsiK_Pos_DTF_PV_Probe0Brem_pbin{}".format(ibin)).getAlgorithm()
        # negative tag (probe 1 Brem)
        , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                    ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                    20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Pos_Probe1Brem + pcut_pos, histname = "BJpsiK_Pos_DTF_PV_Probe1Brem_pbin{}".format(ibin)).getAlgorithm()
        # positive tag (probe 0 Brem)
        , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                    ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                    20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_Probe0Brem + pcut_neg, histname = "BJpsiK_Neg_DTF_PV_Probe0Brem_pbin{}".format(ibin)).getAlgorithm()
        # positive tag (probe 1 Brem)
        , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                    ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                    20*10, 4300, 5800, cut + Jpsiee + StdCut.Jpsiee_Neg_Probe1Brem + pcut_neg, histname = "BJpsiK_Neg_DTF_PV_Probe1Brem_pbin{}".format(ibin)).getAlgorithm()
    ]
    sequence = sequence + addhistos

print sequence

dv.MoniSequence = sequence

#dv.Input         = [
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001238_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001239_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001241_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001242_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001243_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001244_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001245_2.fullturbo.dst', 
#'root://eoslhcb.cern.ch//eos/lhcb/user/p/poluekt/PID/2018_TurCal_DST/00075638_00001246_2.fullturbo.dst', 
#]
