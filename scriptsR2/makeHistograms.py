from TeslaTools.TeslaMonitor import TeslaH1, TeslaH2, TeslaH3
from Configurables import DaVinci
from PidCalibProduction import StandardOfflineRequirements as StdCut

cut         = StdCut.TrackGhostProb
DstCut      = StdCut.DstCut
PosId       = StdCut.PosId
NegId       = StdCut.NegId
Jpsiee      = StdCut.Jpsiee
Jpsi_Pos    = StdCut.Jpsi_Pos
Jpsi_Neg    = StdCut.Jpsi_Neg
BJpsi_Pos   = StdCut.BJpsi_Pos
BJpsi_Neg   = StdCut.BJpsi_Neg
Dsphi_Pos   = StdCut.Dsphi_Pos
Dsphi_Neg   = StdCut.Dsphi_Neg
Dsphi       = StdCut.Dsphi  


dv          = DaVinci()

dv.MoniSequence =  [

#============ Kaons - Pions ===============================#

    # Primary kaons-pions: DSt vs Dz Pos
    # positive tag                                                      
    TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + PosId,
              histname = "DstDz_Pos").getAlgorithm()
    # DSt vs Dz Neg
    # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + NegId, 
              histname = "DstDz_Neg").getAlgorithm()

    # Secondary pions: KS0
    # positive tag                                                      
    , TeslaH1 ( "Hlt2PIDKs2PiPiLLTurboCalib:KS0:M",                       
                ";m(#pi^{+}#pi^{-}) [MeV/c^{2}]; Candidates",                               
                1000, 470, 525, cut, histname = "KS0").getAlgorithm() 

    # Secondary kaons: D_s+ -> phi(1020) pi+               
    # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, cut + PosId, 
                histname = "DsPhiKK_Neg").getAlgorithm()
                                                                                        
    # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, cut + NegId, 
                histname = "DsPhiKK_Pos").getAlgorithm()

#============ Muons ===============================#

    # Primary Muons: Jpsi
    # Negative Tag (positive probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
                1000, 3096 - 110, 3096 + 110,cut+Jpsi_Pos, histname = "Jpsi_Pos").getAlgorithm()
    # Positive Tag (negative probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
                1000, 3096 - 110, 3096 + 110,cut+Jpsi_Neg, histname = "Jpsi_Neg").getAlgorithm()
    
    
    # Secondary muons: B -> J/psi (mumu) K
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut+BJpsi_Neg, histname = "BJpsi_Neg").getAlgorithm()
    
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut+BJpsi_Pos, histname = "BJpsi_Pos").getAlgorithm()
    
    #Secondary muons: Ds -> Phi(mu mu) pi
    # positive tag                                                    
    , TeslaH1 ( "Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalib:D_s+:M",
                ";m(#phi#pi^{+}) [MeV/c^{2}]; Candidates",
                40*10, 1000, 1039, 140*5, 1900, 2040, cut + PosId + Dsphi_Pos + Dsphi,
                histname = "DsPhiMuMu_Neg").getAlgorithm()
    
    # negative tag                                                      
    , TeslaH1 ( "Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalib:D_s+:M",
                ";m(#phi#pi^{+}) [MeV/c^{2}]; Candidates",
                40*10, 1000, 1039, 140*5, 1900, 2040, cut + NegId + Dsphi_Neg + Dsphi,
                histname = "DsPhiMuMu_Pos").getAlgorithm()


#============ Protons ===============================#

    # Lambda0
    # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0_Pos").getAlgorithm()
    # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0HPT_Pos").getAlgorithm()
    # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0VHPT_Pos").getAlgorithm()

    # Lambda~0
    # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0_Neg").getAlgorithm()
    # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0HPT_Neg").getAlgorithm()
    # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0VHPT_Neg").getAlgorithm()


    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "LbLc_Pos" ).getAlgorithm()

    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "LbLc_Neg" ).getAlgorithm()

    # Lambda_c -> ^p K pi (inclusive)
    , TeslaH1 ( "Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "Lc_Pos" ).getAlgorithm()

    # Lambda_c -> ^p K pi (inclusive)
    , TeslaH1 ( "Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "Lc_Neg" ).getAlgorithm()

#============ Electrons ===============================
    
    # B -> J/psi (ee) K
    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Pos").getAlgorithm()

    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Neg").getAlgorithm()

    # B -> J/psi (ee) K USING DELTA MASS
    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M-CHILD(M,1):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_deltaPos").getAlgorithm()

    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M-CHILD(M,1):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_deltaNeg").getAlgorithm()

    ####### DTF mass w/o contraints --> to "solve" (it simply recovers Brem) the electron issue in 2016 #########

    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,False):DTF_FUN(CHILD(1,M),False)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Pos_DTF").getAlgorithm()
    
    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,False):DTF_FUN(CHILD(1,M),False)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Neg_DTF").getAlgorithm()            

    # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,False)-DTF_FUN(CHILD(1,M),False):DTF_FUN(CHILD(1,M),False)",
               ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
               20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
               cut + Jpsiee, histname = "BJpsi_deltaPos_DTF").getAlgorithm()

    # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,False)-DTF_FUN(CHILD(1,M),False):DTF_FUN(CHILD(1,M),False)",
               ";m(J/#psi K^{+}) [MeV/c^{2}]; m(ee) [MeV/c^{2}]",
               20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
               cut + Jpsiee, histname = "BJpsi_deltaNeg_DTF").getAlgorithm()
                    
    ####### DTF mass w Jpsi and PV contraints --> Needed for the 1D fit #########

    # negative tag
    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee, histname = "BJpsiK_Pos_DTF_PV").getAlgorithm()

    # positive tag
    , TeslaH1 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:DTF_FUN(M,True,'J/psi(1S)')",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; Candidates",
                20*10, 4300, 5800, cut + Jpsiee, histname = "BJpsiK_Neg_DTF_PV").getAlgorithm()
    
]





