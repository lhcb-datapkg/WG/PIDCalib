from Configurables import DaVinci
import os

DaVinci().DataType = '2016'

from Configurables import SWeightsTableFiles
SWeightsTableFiles(
    sTableMagUpFile  = '', 
    sTableMagDownFile= os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2016MagDownLeadp.root') 


