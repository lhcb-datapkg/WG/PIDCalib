from Configurables import DaVinci
import os

DaVinci().DataType = '2015'

from Configurables import SWeightsTableFiles
SWeightsTableFiles(sTableMagUpFile  = os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2015MagUp.root', 
                   sTableMagDownFile= os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2015MagDown.root') 


