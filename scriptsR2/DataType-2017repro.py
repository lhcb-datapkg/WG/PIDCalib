from Configurables import DaVinci
import os

DaVinci().DataType = '2017'

from Configurables import SWeightsTableFiles
SWeightsTableFiles(sTableMagUpFile  = os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2017MagUpRepro.root',
                   sTableMagDownFile= os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2017MagDownRepro.root')

